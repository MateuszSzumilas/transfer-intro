const STATUS = document.getElementById('status');
const VIDEO = document.getElementById('webcam');
const ENABLE_CAM_BUTTON = document.getElementById('enableCam');
const RESET_BUTTON = document.getElementById('reset');
const TRAIN_BUTTON = document.getElementById('train');
const MOBILE_NET_INPUT_WIDTH = 224;
const MOBILE_NET_INPUT_HEIGHT = 224;
const STOP_DATA_GATHER = -1;
const CLASS_NAMES = [];
const CLASS_CODES = [];
const MODEL_EPOCHS = 10;

let mobilenet = undefined;
let gatherDataState = STOP_DATA_GATHER;
let videoPlaying = false;
let trainingDataInputs = [];
let trainingDataOutputs = [];
let examplesCount = [];
let predict = false;

ENABLE_CAM_BUTTON.addEventListener('click', enableCam);
TRAIN_BUTTON.addEventListener('click', trainAndPredict);
RESET_BUTTON.addEventListener('click', reset);

let dataCollectorButtons = document.querySelectorAll('button.dataCollector');
for (let i = 0; i < dataCollectorButtons.length; i++) {
    dataCollectorButtons[i].addEventListener('click', gatherDataForClass);
	dataCollectorButtons[i].style.width = MOBILE_NET_INPUT_WIDTH + "px";
    // Populate the human readable names for classes.
    CLASS_NAMES.push(dataCollectorButtons[i].getAttribute('data-name'));
    CLASS_CODES.push(parseInt(dataCollectorButtons[i].getAttribute('data-1hot')));
}

let dataCollectorPhotos = document.querySelectorAll('canvas.photo');
for (let i = 0; i < dataCollectorPhotos.length; i++) {
    dataCollectorPhotos[i].width = MOBILE_NET_INPUT_WIDTH;
    dataCollectorPhotos[i].height = MOBILE_NET_INPUT_HEIGHT;
	dataCollectorPhotos[i].getContext("2d").fillStyle = "blue";
	dataCollectorPhotos[i].getContext("2d").fillRect(0, 0, MOBILE_NET_INPUT_WIDTH, MOBILE_NET_INPUT_HEIGHT);
	dataCollectorPhotos[i].style.border = "5px solid #0000ff";
}

function enableAllCollectorButtons() {
    for (let i = 0; i < dataCollectorButtons.length; i++) {
        dataCollectorButtons[i].disabled = false;
    }
}

function disableOtherCollectorButtons(onlyActiveDataName) {
    for (let i = 0; i < dataCollectorButtons.length; i++) {
        if (dataCollectorButtons[i].getAttribute('data-name') == onlyActiveDataName)
            continue;
        dataCollectorButtons[i].disabled = true;
    }
}

/**
 *	MQTT
 **/

// utw�rz now� instancj� Klienta
let MQTTbroker = "broker.hivemq.com", MQTTport = 8884;
let tmpID = "mchtr_transfer/" + Math.round(Math.random()* 1000);
let tmpTopic = "/" + tmpID;
client = new Paho.MQTT.Client(MQTTbroker, MQTTport, "/mqtt", tmpID);

// po utracie po��czenia
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
    }
}

// po odebraniu wiadomo�ci
function onMessageArrived(message) {
    console.log("onMessageArrived:" + message.payloadString);
}

// ustaw uchwyty funkcji callback w obiekcie Klienta
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// po ustanowieniu po��czenia
function onConnect() {
    console.log("onConnect, ID:" + tmpID);
    //wy�lij wiadomo��
    message = new Paho.MQTT.Message("Hello MQTT!");
    message.destinationName = tmpTopic + "/status";
    client.send(message);
    console.log("publishing to \"" + tmpTopic + "\"");
}

// w razie niepowodzenia lub przekroczenia czasu oczekiwania
function doFail(e) {
    console.log(e);
}

// nawi�� po��czenie
client.connect({
    onSuccess: onConnect,
    onFailure: doFail,
	useSSL: true
});

/**
 *	TFJS
 **/

/**
 * Loads the MobileNet model and warms it up so ready for use.
 **/
async function loadMobileNetFeatureModel() {
    const URL =
        'https://tfhub.dev/google/tfjs-model/imagenet/mobilenet_v3_small_100_224/feature_vector/5/default/1';

    STATUS.innerText = String.fromCodePoint(0x23EC) + " loading graph...";
	
    disableOtherCollectorButtons(null);
    mobilenet = await tf.loadGraphModel(URL, {
        fromTFHub: true
    });
    STATUS.innerText = 'MobileNet v3 loaded successfully!';
	
    // Warm up the model by passing zeros through it once.
    tf.tidy(function () {
        let answer = mobilenet.predict(tf.zeros([1, MOBILE_NET_INPUT_HEIGHT, MOBILE_NET_INPUT_WIDTH, 3]));
        console.log(answer.shape);
    });
	
    STATUS.innerText += String.fromCodePoint(0x1F525) + " Model warmed up!";
}

// Call the function immediately to start loading.
loadMobileNetFeatureModel();

let model = tf.sequential();
model.add(tf.layers.dense({
        inputShape: [1024],
        units: 128,
        activation: 'relu'
    }));
model.add(tf.layers.dense({
        units: CLASS_NAMES.length,
        activation: 'softmax'
    }));

model.summary();

// Compile the model with the defined optimizer and specify a loss function to use.
model.compile({
    optimizer: 'adam',
    loss: (CLASS_NAMES.length === 2) ? 'binaryCrossentropy' : 'categoricalCrossentropy',
    metrics: ['accuracy']
});

/**
 * Handle Data Gather
 **/
function gatherDataForClass() {
    let classNumber = parseInt(this.getAttribute('data-1hot'));
    if (gatherDataState === STOP_DATA_GATHER) {
        gatherDataState = classNumber;
        disableOtherCollectorButtons(this.getAttribute('data-name'));
        dataGatherLoop();
    } else {
        gatherDataState = STOP_DATA_GATHER;
    }
}

function hasGetUserMedia() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}

function enableCam() {
    if (hasGetUserMedia()) {
        // getUsermedia parameters.
        const constraints = {
            video: true,
            width: 640,
            height: 480
        };

        // Activate the webcam stream.
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
            VIDEO.srcObject = stream;
            VIDEO.addEventListener('loadeddata', function () {
                videoPlaying = true;
                ENABLE_CAM_BUTTON.classList.add('removed');
            });
        });

        enableAllCollectorButtons();
    } else {
        console.warn('getUserMedia() is not supported by your browser');
    }
}

function dataGatherLoop() {
    if (videoPlaying && gatherDataState !== STOP_DATA_GATHER) {
        let imageFeatures = tf.tidy(function () {
            let videoFrameAsTensor = tf.browser.fromPixels(VIDEO);
            let resizedTensorFrame = tf.image.resizeBilinear(videoFrameAsTensor, [MOBILE_NET_INPUT_HEIGHT,
                        MOBILE_NET_INPUT_WIDTH], true);
            let normalizedTensorFrame = resizedTensorFrame.div(255);
            return mobilenet.predict(normalizedTensorFrame.expandDims()).squeeze();
        });

        trainingDataInputs.push(imageFeatures);
        trainingDataOutputs.push(gatherDataState);
		
		for (let i = 0; i < dataCollectorPhotos.length; i++) {
		    if (parseInt(dataCollectorPhotos[i].getAttribute("data-1hot")) == gatherDataState)
				dataCollectorPhotos[i].getContext('2d')
				.drawImage(VIDEO, 0, 0, dataCollectorPhotos[i].width, dataCollectorPhotos[i].height);
		}

        // Intialize array index element if currently undefined.
        if (examplesCount[gatherDataState] === undefined) {
            examplesCount[gatherDataState] = 0;
        }
        examplesCount[gatherDataState]++;

        STATUS.innerText = '';
        for (let n = 0; n < CLASS_NAMES.length; n++) {
            STATUS.innerText += CLASS_NAMES[n] + ' data count: ' + examplesCount[n] + '\n';
        }
        window.requestAnimationFrame(dataGatherLoop);
    } else {
        enableAllCollectorButtons();
    }
}

async function trainAndPredict() {
	
    STATUS.innerText = String.fromCodePoint(0x1F393) + " Training..." + String.fromCodePoint(0x2615);
		
	try {	
		predict = false;
		tf.util.shuffleCombo(trainingDataInputs, trainingDataOutputs);
		let outputsAsTensor = tf.tensor1d(trainingDataOutputs, 'int32');
		let oneHotOutputs = tf.oneHot(outputsAsTensor, CLASS_NAMES.length);
		let inputsAsTensor = tf.stack(trainingDataInputs);

		let results = await model.fit(inputsAsTensor, oneHotOutputs, {
			shuffle: true,
			batchSize: 5,
			epochs: MODEL_EPOCHS,
			callbacks: {
				onEpochEnd: logProgress
			}
		});
		
		outputsAsTensor.dispose();
		oneHotOutputs.dispose();
		inputsAsTensor.dispose();
		predict = true;
		
	} catch(error)  {
		console.error(error);
		STATUS.innerText = String.fromCodePoint(0x1F625) + " Training failed...";
	}
	
	if (predict == true) predictLoop();
}

function logProgress(epoch, logs) {
    console.log('Data for epoch ' + epoch, logs);
    STATUS.innerText = String.fromCodePoint(0x1F393) + " Training..." + String.fromCodePoint(0x2615) +
	" epoch: " + (epoch + 1) + "/" + MODEL_EPOCHS + " (acc:" + logs.acc.toFixed(3) + ")";
}

function predictLoop() {
    if (predict) {
        tf.tidy(function () {
            let videoFrameAsTensor = tf.browser.fromPixels(VIDEO).div(255);
            let resizedTensorFrame = tf.image.resizeBilinear(videoFrameAsTensor, [MOBILE_NET_INPUT_HEIGHT,
                        MOBILE_NET_INPUT_WIDTH], true);

            let imageFeatures = mobilenet.predict(resizedTensorFrame.expandDims());
            let prediction = model.predict(imageFeatures).squeeze();
            let highestIndex = prediction.argMax().arraySync();
            let predictionArray = prediction.arraySync();

            STATUS.innerText = 'Prediction: ' + CLASS_NAMES[highestIndex] + ' with ' + Math.floor(predictionArray[highestIndex] * 100) + '% confidence';

			for (let i = 0; i < dataCollectorPhotos.length; i++) {
				if (parseInt(dataCollectorPhotos[i].getAttribute("data-1hot")) == CLASS_CODES[highestIndex])
					dataCollectorPhotos[i].style.border = "5px solid #ffaa00";
				else 
					dataCollectorPhotos[i].style.border = "5px solid #000000";
			}

			try {
				message = new Paho.MQTT.Message(CLASS_NAMES[highestIndex]);
				message.destinationName = tmpTopic + "/class";
				client.send(message);
			} catch (error) {
				console.error(error);
				try {
					client.connect({
						onSuccess: onConnect,
						onFailure: doFail,
						useSSL: true
					});
				} catch (error) {
					console.error(error);
				}
			}
        });

        window.requestAnimationFrame(predictLoop);
    }
}

/**
 * Purge data and start over. Note this does not dispose of the loaded
 * MobileNet model and MLP head tensors as you will need to reuse
 * them to train a new model.
 **/
function reset() {
    predict = false;
    examplesCount.length = 0;
    for (let i = 0; i < trainingDataInputs.length; i++) {
        trainingDataInputs[i].dispose();
    }
    trainingDataInputs.length = 0;
    trainingDataOutputs.length = 0;
	
	for (let i = 0; i < dataCollectorPhotos.length; i++) {
		dataCollectorPhotos[i].getContext("2d").fillStyle = "blue";
		dataCollectorPhotos[i].getContext("2d").fillRect(0, 0, MOBILE_NET_INPUT_WIDTH, MOBILE_NET_INPUT_HEIGHT);
		dataCollectorPhotos[i].style.border = "5px solid #0000ff";
	}
	
    STATUS.innerText = 'No data collected';

    console.log('Tensors in memory: ' + tf.memory().numTensors);
}
